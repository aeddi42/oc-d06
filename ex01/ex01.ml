module StringHash =
    struct
        type t = string
        let equal x y = (x = y)
        let hash x = 
            let rec djb2 count acc =
                if count = String.length x then acc
                else djb2 (count + 1) (((acc lsl 5) + acc) + (int_of_char (String.get x count)))
            in
            djb2 0 5381
    end

module StringHashtbl = Hashtbl.Make (StringHash)

let () =
    let ht = StringHashtbl.create 5 in
    let values = [ "Hello"; "world"; "42"; "Ocaml"; "H" ] in
    let pairs = List.map (fun s -> (s, String.length s)) values in
    List.iter (fun (k,v) -> StringHashtbl.add ht k v) pairs;
    StringHashtbl.iter (fun k v -> Printf.printf "k = \"%s\", v = %d\n" k v) ht
